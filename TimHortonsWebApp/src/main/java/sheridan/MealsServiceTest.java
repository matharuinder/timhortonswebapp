package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	@Test
	public void testDrinksRegular() {
		MealsService ms = new MealsService();
		
		List<String> test = ms.getAvailableMealTypes(MealType.DRINKS);
		
		int length = test.size();
		assertTrue(length!=0);
		
	}
	
	@Test
	public void testDrinksException() {
		MealsService ms = new MealsService();
		
		List<String> test = ms.getAvailableMealTypes(null);
		
		assertTrue(test.get(0).equals("No Brand Available"));
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		MealsService ms = new MealsService();
		
		List<String> test = ms.getAvailableMealTypes(MealType.DRINKS);
		
		int length = test.size();
		assertTrue(length>3);
	}
	
	
	@Test
	public void testDrinksBoundaryOut() {
		MealsService ms = new MealsService();
		
		List<String> test = ms.getAvailableMealTypes(null);
		
		int length = test.size();
		assertTrue(length<=1);
	}
	

}
